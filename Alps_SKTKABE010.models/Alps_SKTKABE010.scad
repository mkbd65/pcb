$fn=16;

height=0.78;

union() {
    color("black") {
        linear_extrude(height=0.2) {
            polygon([[-2.95, 1.15], [-2, 2], [2, 2], [2.95, 1.15], [2.95, -1.15], [2, -2], [-2, -2], [-2.95, -1.15]]);
        }
    }
    color("white") {
        translate([0, 0, 0.2]) {
            linear_extrude(height=0.2) {
                polygon([[-2.95, 0.75], [-1.55, 2], [1.55, 2], [2.95, 0.75], [2.95, -0.75], [1.55, -2], [-1.55, -2], [-2.95, -0.75]]);
            }
        }
    }
    color("black") {
        translate([0, 0, height - 0.4]) {
            cylinder(h=0.38, d=1.2);
        }
    }
    color("silver") {
        translate([-3.1, 0.75]) {
            cube([0.15, 0.4, 0.1]);
        }
        translate([-3.1, -1.15]) {
            cube([0.15, 0.4, 0.1]);
        }
        translate([2.95, 0.75]) {
            cube([0.15, 0.4, 0.1]);
        }
        translate([2.95, -1.15]) {
            cube([0.15, 0.4, 0.1]);
        }
    }
}