NOTES
=====

mkbd65 is a CTVPD (Charge Through VCONN Powered Device)

## Charging requirements

### Samsung bullshit lingo

- Charging
  - 5V
- Fast Charging
  - 5V, 1.5A?
  - Charger with 15W?
- "Super Fast Charging 2.0"
  - Charger with 45W at 9V/5A PPS
  - 5A (100W) cable

## Notes for PCB

- **`VBUS` must be 5A capable!**
