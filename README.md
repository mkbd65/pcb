# mkbd65 PCB

This repository contains the KiCAD files for the main PCB of mkbd65. You'll also need a connector cable that is
compatible with your phone and a case to fully assemble mkbd65. Follow the build guide at https://mkbd65.xengi.de for
build instructions.

---

**old readme**

<img src="https://gitlab.com/mkbd65/pcb/-/raw/main/mkbd65_front.png?inline=false" width="900"/>
<img src="https://gitlab.com/mkbd65/pcb/-/raw/main/mkbd65_back.png?inline=false" width="900"/>

# mkbd65 PCB

These are the KiCAD files for the main PCB. There is also a flat cable PCB you'll need to fully assemble mkbd65.

The `main` folder contains the main PCB which holds all components. The `connector` folder contains the flat cables to connect to your phone. You have to build/order the correct one for your phone. If your phone is not supported open a ticket [here](https://gitlab.com/mkbd65/case/-/issues/new?issue#) with the `Add phone` template to get it on the list.

Get the gerber files for the main PCB and your phones connector cable from the latest release:

- [rev0][rev0]
- connector oneplus 8t rev0 (TBA)

__IMPORTANT__: When ordering the main PCB make sure that the PCB thickness is set to 1mm. This is needed so that the PCB fits into the case.

Ordering 5 PCBs with a PCB fab like [JLCPCB](https://jlcpcb.com/) will cost around ~7€ + ~5€ shipping to Germany. If you want golden ENIG finsh and a nice purple color you will end up with ~25€. You can check the cost of the other components with the [Octopart BOM][octopart].

_TIP: If you can't find the ATMEGA32, you can sample it directly from Microchip. 😉_

A stencil to make it easier to apply solder paste can be ordered for ~6€. You can then solder components with a [hot air gun](https://duckduckgo.com/?q=hot+air+gun+soldering&t=h_&iar=images&iax=images&ia=images) or a [reflow oven](https://duckduckgo.com/?q=reflow+oven&t=h_&iax=images&ia=images).

I soldered my own board with a T-962 reflow oven with [custom firmware](https://github.com/UnifiedEngineering/T-962-improvements).

Using the [Interactive BOM][ibom] makes it easy to place components once you got your PCBs.

## Firmware

1. Get the QMK firmware
   ```shell
   git clone https://gitlab.com/mkbd65/qmk-firmware.git
   ```
2. Checkout the `mkbd65` branch
   ```
   cd qmk-firmware
   git checkout mkbd65
   ```
3. Install [qmk tool](https://docs.qmk.fm/#/cli)
   ```shell
   # Arch Linux
   sudo pacman -S qmk
   # Other Linux
   pip install --user qmk
   # macOS
   brew install qmk/qmk/qmk
   ```
   Windows: https://msys.qmk.fm/
4. Setup qmk environment
   ```shell
   qmk setup
   ```
3. Flash firmware (change `ansi` to `iso` for different layout):
   ```shell
   qmk flash -kb mkbd65 -km ansi
   ```

You can find the source code for the keyboard under [keyboards/mkbd65](https://gitlab.com/mkbd65/qmk-firmware/-/tree/mkbd65/keyboards/mkbd65) in the repo.

## Case

The case for mkbd65 can be found [here](https://gitlab.com/mkbd65/case). It is available for a limited number of smartphones but can easily be adapted to more.

Follow the instructions to build it for your phone.

---

<img src="https://qmk.fm/assets/images/badge-small-dark.svg" alt="powered by QMK" height="50" /> ![CC BY-SA 4.0](https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg)
<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/mkbd65/pcb">mkbd65 PCB</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://mkbd65.xengi.de">XenGi</a> is licensed under <a href="http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-SA 4.0</a></p>


[octopart]: https://octopart.com/bom-tool/YlMBpo3w
[ibom]: https://gitlab.com/mkbd65/pcb/-/raw/main/bom/ibom.html?inline=false
[rev0]: https://gitlab.com/mkbd65/pcb/-/releases/rev0
[connector_oneplus_8t_rev0]: https://gitlab.com/mkbd65/pcb/-/releases/connector_oneplus_8t_rev0
[qmk_toolbox]: https://github.com/qmk/qmk_toolbox
